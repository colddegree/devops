#!/bin/sh

cd /app/dev

# пометить сборку временной меткой
CURRENT_TIMESTAMP=$(date +%s)
mkdir $CURRENT_TIMESTAMP
NEW_DEV_DIR=/app/dev/$CURRENT_TIMESTAMP

# распаковать артефакт
tar -xvf tmp/artifact.tar.bz2 -C $NEW_DEV_DIR

# скопировать .env
cp .env $NEW_DEV_DIR

# дать права на артефакт
chown -R :docker $NEW_DEV_DIR

cd $NEW_DEV_DIR/docker

# перезапустить контейнеры
docker-compose down
docker-compose up -d

# создать базу и накатить миграции
bin/console doctrine:database:create --if-not-exists
bin/console doctrine:migrations:migrate --no-interaction

# инициализировать var
bin/console cache:clear

# пометить симлинком текущий dev (для удобства)
ln -snf $NEW_DEV_DIR /app/dev/current_dev

# удалить временные файлы
rm -rf /app/dev/tmp /app/dev/before.sh /app/dev/after.sh
