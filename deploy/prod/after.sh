#!/bin/sh

cd /app/prod

# пометить сборку временной меткой
CURRENT_TIMESTAMP=$(date +%s)
mkdir $CURRENT_TIMESTAMP
NEW_PROD_DIR=/app/prod/$CURRENT_TIMESTAMP

# распаковать артефакт
tar -xvf tmp/artifact.tar.bz2 -C $NEW_PROD_DIR

# скопировать .env
cp .env $NEW_PROD_DIR

# дать права на артефакт
chown -R :docker $NEW_DEV_DIR

cd $NEW_PROD_DIR

# инициализировать var
bin/console cache:clear

# перекинуть симлинк на новый prod
ln -snf $NEW_PROD_DIR /app/prod/current_docroot

# удалить временные файлы
rm -rf docker /app/prod/tmp /app/prod/before.sh /app/prod/after.sh
